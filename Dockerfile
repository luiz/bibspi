FROM openjdk:7-jdk
MAINTAINER 1science Devops Team <devops@1science.org>

# Environment variables
ENV DSPACE_VERSION=3.1 TOMCAT_MAJOR=8 TOMCAT_VERSION=8.0.44
ENV TOMCAT_TGZ_URL=https://www.apache.org/dist/tomcat/tomcat-8/v8.0.44/bin/apache-tomcat-8.0.44.tar.gz \
    MAVEN_TGZ_URL=http://apache.mirror.iweb.ca/maven/maven-3/3.5.0/binaries/apache-maven-3.5.0-bin.tar.gz 
ENV CATALINA_HOME=/usr/local/tomcat DSPACE_HOME=/dspace
ENV PATH=$CATALINA_HOME/bin:$DSPACE_HOME/bin:$PATH
ENV CATALINA_OPTS="-Djava.awt.headless=true -Dfile.encoding=UTF-8 \
-server -Xms2048m -Xmx2048m \
-XX:NewSize=256m -XX:MaxNewSize=256m -XX:PermSize=256m \
-XX:MaxPermSize=256m -XX:+DisableExplicitGC" 

WORKDIR /tmp

# Install runtime and dependencies
RUN apt-get update && apt-get install -y vim ant postgresql-client
RUN mkdir -p maven dspace "$CATALINA_HOME"
RUN curl -fSL "$TOMCAT_TGZ_URL" -o tomcat.tar.gz
RUN curl -fSL "$MAVEN_TGZ_URL" -o maven.tar.gz
RUN tar -xvf tomcat.tar.gz --strip-components=1 -C "$CATALINA_HOME"
RUN tar -xvf maven.tar.gz --strip-components=1  -C maven
ADD ./dspace /tmp/dspace
RUN pwd \
    && cd dspace && ../maven/bin/mvn package \
    && cd dspace/target/dspace-3.1-build \
    && ant init_installation init_configs install_code copy_webapps \
    && rm -fr "$CATALINA_HOME/webapps" && mv -f /dspace/webapps/ "$CATALINA_HOME"/webapps/ \
    && rm -fr ~/.m2 && rm -fr /tmp/* 

ADD ./rootfs /

WORKDIR /dspace

EXPOSE 8080
CMD ["start-dspace"]
